// path of the ressources
var rsc = path.join(process.resourcesPath, 'resources')

// exports for external access of functions
exports.load_locale = (locale_name, translate) => load_locale(locale_name, translate)
exports.translate_all = () => translate_all()

// the active localization file
exports.locale = {}

var locales_list =  {}

// initiliaze this script
init()

// Sets for every element with a data-lang tag the localized string
function translate_all() {
	// Array of all elements with the data-lang_text tag
	var elements_text = Array.from(document.querySelectorAll("*[data-lang_text]"))

	// Array of all elements with the data-lang_placeholder tag
	var elements_placeholder = Array.from(document.querySelectorAll("*[data-lang_placeholder]"))
	
	// set for each element in the lists the localized value from the locale-files
	elements_text.forEach(e => {
		const key = e.dataset.lang_text

		t(e, key)
	})

	elements_placeholder.forEach(e => {
		const key = e.dataset.lang_placeholder

		t(e, key, "placeholder")
	})
}

// loads a new locale-file and translates the program-elements
function load_locale (locale_name, translate = true) {
	exports.locale = require(path.join(rsc, 'locale', locale_name))
	update_locale(exports.locale) // update the locale in the main script

	if (translate) {
		var date_range = start_calendar.getRange()

		refresh_calendars(date_range)
		translate_all()
	}
}

// sets the localized text for the given element
function t(e, key, type = "text") {
	e.innerText = exports.locale[key]

	switch (type) {
		case "text":
			e.innerText = exports.locale[key]
			break
		case "placeholder":
			e.placeholder = exports.locale[key]
	}
}

// Initilize this script
function init () {
	var locale_files = fs.readdirSync(path.join(rsc, 'locale'))

	locale_files.forEach(file => {
		var temp_locale = require(path.join(rsc, 'locale', file))
	
		locales_list[temp_locale["lang_name"]] = file
	})

	load_locale(settings['locale'], false)
}