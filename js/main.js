const fs = require('fs');
const datepicker = require('js-datepicker');
const Inkscape = require('inkscape')
const Jimp = require('jimp')
const temp = require('temp').track()
const { dialog } = require('@electron/remote')
const path = require('path');
const os = require('os');
const { exception } = require('console');

// save the directory of the executable (elelectron-builder constant)
if (process.env.APPIMAGE) {
	var home = path.dirname(process.env.APPIMAGE)
} else if (process.env.PORTABLE_EXECUTABLE_DIR) {
	home = process.env.PORTABLE_EXECUTABLE_DIR
}

// path of the ressources
var rsc = path.join(process.resourcesPath, 'resources')

const settings_js = require(path.join('..', 'js', 'settings'))
var { settings } = require(path.join('..', 'js', 'settings'))
const locale_js = require(path.join('..', 'js', 'set_locale'))
locale = locale_js.locale

// variables for the html-elements
var preview_svg = document.getElementById('preview_svg');
var template_path_box = document.getElementById('template_path');
var title_box = document.getElementById('template_name')
var extra_dates_box = document.getElementById('extra_dates_input')
var extra_dates_list_box = document.getElementById('extra_dates_div_list')

// stores the extra-dates and the which is selected
var extra_dates = []
var date_selected = -1

// the date from today (execution of this line of code) to have a common reference
const today = new Date();

// stores the string of the currently loaded svvg
var svg_string;

// options for the calendars
var start_calendar_options
var stop_calendar_options
// variables of the datepickers
var start_calendar
var stop_calendar

// dictionary of special-chars and their html replacements
const special_char_dic = {
	"Ä": "&#196;",
	"ä": "&#228;",
	"Ë": "&#203;",
	"ë": "&#235;",
	"Ï": "&#207;",
	"ï": "&#239;",
	"Ö": "&#214;",
	"ö": "&#246;",
	"Ü": "&#220;",
	"ü": "&#252;",
	"ß": "&#223;",
	"À": "&#192;",
	"à": "&#224;",
	"Á": "&#193;",
	"á": "&#225;",
	"Â": "&#194;",
	"â": "&#226;",
	"Ç": "&#199;",
	"ç": "&#231;",
	"È": "&#200;",
	"è": "&#232;",
	"É": "&#201;",
	"é": "&#233;",
	"Ê": "&#202;",
	"ê": "&#234;",
	"Ñ": "&#209;",
	"ñ": "&#241;",
	"Ò": "&#210;",
	"ò": "&#242;",
	"Ó": "&#211;",
	"ó": "&#243;",
	"Ô": "&#212;",
	"ô": "&#244;",
	"õ": "&#245;",
	"Ÿ": "&#195;",
	"ÿ": "&#255;"
}

// initilize this script
init()

// adds a leading zero to a number to make it two digits long
function two_digit_date (raw_date) {
	if (raw_date < 10) {
		return "0" + raw_date;
	} else {
		return raw_date;
	}
}

// returns a formatted string from a date
function create_date_string(raw_date) {
	const replace_keys = {
		"d": raw_date.getDate(),
		"dd": two_digit_date(raw_date.getDate()),
		"D": locale['days_short'][raw_date.getDay()],
		"DD": locale['days'][raw_date.getDay()],
		"m": raw_date.getMonth() + 1,
		"mm": two_digit_date(raw_date.getMonth() + 1),
		"MM": locale['months'][raw_date.getMonth()],
		"yyyy": raw_date.getFullYear()
	}

	date_string = settings['date_string']

	for (var key in replace_keys) {
		date_string = date_string.replaceAll('<' + key + '>', replace_keys[key])
	}

	return date_string
}

// returns a iso-formatted string from a date: yyyy-mm-dd
function create_date_string_iso(raw_date) {
	return raw_date.getFullYear() + "-" + two_digit_date(raw_date.getMonth() + 1) + "-" + two_digit_date(raw_date.getDate());
}

// export-button was clicked
function export_btn_clicked() {
	var export_dates = [];
	var export_dir = dialog.showOpenDialogSync({ // let the user choose a export directory - TODO: load the last used directory
		title: "Choose export directory",
		defaultPath: settings["last_export_dir"],
		properties: ['openDirectory']
	})[0]
	// store the chosen directory in the settings file
	settings["last_export_dir"]	= export_dir


	// check if a stop_date is selected
	if (stop_calendar.dateSelected !== undefined) {
			// Declare the variables empty and then read the values from the pickers in a second step, to avoid changing the pickers date while creating the export-array
		const start_date = new Date(start_calendar.dateSelected)
		const stop_date = new Date(stop_calendar.dateSelected)

		for (day in settings['export_days']) {
			if (settings['export_days'][day]) {
				// the next date with with the selected day
				current_date = new Date(start_date.getTime() + (day - start_date.getDay() + 7) % 7 * 24 * 3600 * 1000)


				while (current_date.getTime() <= stop_date.getTime()) {
					// create a temporary-date variable which is then pushed to the array to store the actual date in the array and not the object, which changes with each iteration
					var temp_date = new Date(current_date);
					
					export_dates.push(temp_date);
			
					// set the current date to one week later
					current_date = new Date(temp_date.getTime() + 7 * 24 * 3600 * 1000)
				}
			}
		}
	}

	// Add the dates from the extra-dates-list-box
	var list_elements = extra_dates_list_box.getElementsByTagName('span')
	for (var i = 0; i < list_elements.length; i++) {
		var temp_date = new Date(list_elements[i].innerHTML)

		export_dates.push(temp_date)
	}

	// export all at once
	export_dates.forEach(element => {
		// placeholder to keep the export-date in a formatted form
		const date_string = create_date_string(element);
		const date_string_iso = create_date_string_iso(element);

		// write the current date in a copy of the svg
		const temp_svg_string = svg_string.replaceAll(settings["date_insert_key"], date_string);

		// create a temporary svg file for the png conversion
		temp.open('', (err, temp_file) => {
			if (err) {
				console.error(err)
			} else {
				// write the temporary svg file
				fs.writeFile(temp_file.path, temp_svg_string, (err) => {
					if(err) {
						return console.error(err);
					}

					// path for the png and jpg files
					var png_path = path.join(export_dir, date_string_iso + '.png')
					var jpg_path = path.join(export_dir, date_string_iso + '.jpg')

					// streams for the temp-svg-file and png-file
					svg_pipe = fs.createReadStream(temp_file.path)
					png_pipe = fs.createWriteStream(png_path)

					// inkscape instance with png-export-mode
					svg2pdf = new Inkscape(['--export-type=png'])

					// create pipe-constructor to read svg and write png
					inkscape_piping = svg_pipe.pipe(svg2pdf).pipe(png_pipe)

					// eventlistener to convert png to jpg after svg --> png rendering
					inkscape_piping.on('finish', () => {
						// create jimp instance to convert to jpg
						Jimp.read(png_path, function (err, source_file) {
							if (err) {
								return err
							}

							// saves as jpg
							source_file.quality(95).write(jpg_path)
						})
					})
				});
			}
		});
	});
}

// choose a template file
function choose_file() {
	var template_path = dialog.showOpenDialogSync({
		title: "Open SVG template",
		defaultPath: path.dirname(settings['last_template']),
		properties: ['openFile'],
		filters: [
			{ name: "SVG", extensions: ['svg'] },
			{ name: "All Files", extension: ['*']}
		]
	})[0]

	// store the chosen template in the settings file
	settings["last_template"] = template_path

	load_svg(template_path)
}

// saves a svg-path to the path-box, writes the name to the title and loads the svg into the preview
function load_svg(data_path, silent=false) {
	const template_path = path.join(rsc, 'default-template.svg')

	const load_file = (file_path) => {
		fs.readFile(file_path, "utf8", (err, svg_data) => {
			if (err) {
				return console.error(err)
			}

			title_box.innerHTML = path.basename(file_path, '.svg')
	
			svg_string = unspecialize_string(svg_data)
			preview_svg.innerHTML = svg_string.replaceAll(settings['date_insert_key'], create_date_string(today));	
		})
	}

	// only proceed if it is a svg file 
	if (data_path.toString().split('.').pop() === 'svg') {
		// write the svg-path to the path-box
		template_path_box.value = data_path

		if (fs.existsSync(data_path)) { // check if svg_path exists
			load_file(data_path)
		} else { // svg-file doesn't exist --> alert and show template file
			var error_msg = 'file not found, using default file instead'
			console.log(error_msg)

			if (!silent) {
				alert(console.msg)
			}

			load_file(template_path)
		}
	} else { // not a svg-file, alert user and show template file
		if (!silent) {
			alert("Please choose a SVG file\nShowing default file instead")
		}

		load_file(template_path)
	}
}

// replaces all special chars by html-breakouts for a give string
function unspecialize_string(special_string) {
	for (var k in special_char_dic) {
		special_string = special_string.replaceAll(k, special_char_dic[k])
	}

	return special_string
}

// gets triggered, if the template_path_box gets typed in. catches enter-presses and applies the current path
function template_path_keypress(event) {
	if (event.key === "Enter") {
		load_svg(template_path_box.value)
	}
}

// gets triggered, when the extra_dates_box gets typed in. catches enter-presses and adds the current date
function extra_date_keyup(event) {
	if (event.key === "Enter") {
		add_extra_date()
	}
}

// add a date string to the date list
// default: use the string from the extra_dates_box
function add_extra_date(date_string_iso = extra_dates_box.value) {
	var check_date = new Date(date_string_iso)

	// check if a valid date-string was entered
	if (check_date instanceof Date && !isNaN(check_date)) {
		date_string_iso = create_date_string_iso(check_date)

		// check if the date is already in the list
		var new_element = true;

		for (var i = 0; i < extra_dates.length; i++) {
			new_element = extra_dates[i] !== date_string_iso
			
			if (!new_element) break
		}

		if (new_element) {

			extra_dates.push(date_string_iso)

			extra_dates.sort()

			date_selected = extra_dates.indexOf(date_string_iso)

			refresh_dates_list() // refresh the list in html

			extra_dates_box.focus()
		}

		if (new_element) {
			extra_dates_box.value = ""
		}
	} else if (date_string_iso !== "") // catch an empty date-box	
	{
		alert ("Please enter a valid date")
	}
}

// flushes and refills the extra_dates_list
function refresh_dates_list() {
	extra_dates_list_box.innerHTML = ""

	for (var i = 0; i < extra_dates.length; i++) {
		var date_element = document.createElement('span')
		date_element.classList.add("option")

		date_element.onclick = function() {span_option_click(this)}
		date_element.tabIndex = -1
		date_element.onkeyup = function() {extra_date_keypress(event)}

		date_element.innerHTML = extra_dates[i]

		if (i > 0) {
			var hr_sep = document.createElement('hr')
			hr_sep.classList.add("seperator")

			extra_dates_list_box.appendChild(hr_sep)
		} else {
			date_element.classList.add("option_top")
		}

		extra_dates_list_box.appendChild(date_element)
	}

	if (date_selected != -1) {
		extra_dates_list_box.getElementsByTagName("span")[date_selected].classList.add("selected")
		extra_dates_list_box.getElementsByTagName("span")[date_selected].focus()
	}
}

// remove the selected date form the date list
function remove_extra_date() {
	extra_dates.splice(date_selected, 1)
	
	if (date_selected >= extra_dates.length){
		date_selected--
	}

	refresh_dates_list()
}

// an span.option was selected
function span_option_click (e) {
	date_selected = extra_dates.indexOf(e.innerHTML)

	refresh_dates_list()
}

// initializes a calendar
function init_calendar(id, options) {
	return datepicker(id, options)
}

// refreshes the calendars by deleting the current ones and creating new ones with new options but same date selection
function refresh_calendars(date_range = start_calendar.getRange()) {
	// assume if one calendar is undefined the other is also --> if undefined, don't try to remove an existing calendar
	if (start_calendar_options !== undefined){
		// remove the existing calendars
		start_calendar.remove()
		stop_calendar.remove()
	}
	
	if (settings["custom_week_start_enable"]) {
		var week_start = settings["custom_week_start"]
	} else {
		var week_start = locale["week_start"]
	}

	start_calendar_options = {
		dateSelected: date_range["start"],
	
		alwaysShow: true,
	
		id: 1,
		startDay: week_start,
		customDays: locale["days_short"],
		customMonths: locale["months"]
	}

	stop_calendar_options = {
		dateSelected: date_range["end"],
	
		alwaysShow: true,
	
		id: 1,
		startDay: week_start,
		customDays: locale["days_short"],
		customMonths: locale["months"]
	}

	// create the calendars with the option-dictionaries
	start_calendar = init_calendar("#start_calendar", start_calendar_options)
	stop_calendar = init_calendar("#stop_calendar", stop_calendar_options)

	// add a custom css-class to the datepickers
	var datepickers_container = Array.from(document.getElementsByClassName('qs-datepicker-container'))
	datepickers_container.forEach(datepicker => {
		datepicker.classList.add('datepicker_container')
	})
}

// gets called from set_locale.js to load the fresh locale
function update_locale(locale_new) {
	locale = locale_new
}

// a keypress happened and an extra-date was selected --> check for Key = "Delete"
function extra_date_keypress(e) {
	switch (e.key) {
		case "Delete":
			remove_extra_date()

			break
	}
}

// initializes this script
function init() {
	// automatically track and cleanup temporary files at texit
	temp.track();

	// initialize the calendars; start-date is today
	refresh_calendars({"start": today})

	locale_js.translate_all()
	load_svg(settings["last_template"], true)

	// replace the single-dot home-dir reference with the realpath to prevent windows portables to show in temp-dir
	if (settings['last_export_dir'] === '.') {
		settings['last_export_dir'] = os.homedir()
	}
}