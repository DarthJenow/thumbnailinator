var $ = require('jquery');
const fs = require('fs')
const path = require('path')

// path of the unpacked ressources
var rsc = path.join(process.resourcesPath, 'resources')

// variables for the html-elements
var settings_background_blur = document.getElementById('settings_popup')
var language_selector = document.getElementById('language_selector')
var date_insert_key_input = document.getElementById('date_insert_key_input')
var week_start_selector = document.getElementById('week_start_selector')
var week_start_slider = document.getElementById('week_start_slider')
var date_string_input = document.getElementById('date_string_input')
var reg_export_days_select = document.getElementById('settings_reg_export_days_select')
var reg_export_days_select_dummy = document.getElementById('settings_reg_export_select_dummy')

// all the availabe locale files
var locale_files = fs.readdirSync(path.join(rsc, 'locale'))

// path of the settings file
const settings_path = path.join(home, 'config', 'settings.json')

// if the settings-file doesn't exist yet, create it from the ressources
if (!fs.existsSync(settings_path)) {
	fs.mkdirSync(path.dirname(settings_path), {recursive: true})

	fs.copyFileSync(path.join(rsc, 'settings_template.json'), settings_path)
}
exports.settings = require(path.join(home, 'config', 'settings'))
var settings = exports.settings

// exports for external access of functions
exports.language_changed = (e) => language_changed(e)
exports.open_settings = () => open_settings()
exports.hide_settings = (e, force) => hide_settings(e, force)
exports.init_settings_elements = () => init_settings_elements()
exports.write_settings_to_file = () => write_settings_to_file()
exports.resize_select = (e) => resize_select(e)
exports.week_start_slider_changed = (e) => { week_start_slider_changed(e); refresh_calendars(); init_settings_elements() }
exports.week_start_changed = e => week_start_changed(e)
exports.choose_regular_day = e => choose_regular_day(e)
exports.reg_week_day_selector = e => reg_week_day_selector(e)

// initialize this script
init()

// show the settings
function open_settings() {
	settings_background_blur.style.display = "unset"
}

// hide the settings
function hide_settings(e, force = false) {
	if (e.target === settings_background_blur || force) {
		settings_background_blur.style.display = "None"

		settings["date_insert_key"] = date_insert_key_input.value
		settings['date_string'] = date_string_input.value

		load_svg(settings['last_template'], true)
	}
}

// a language was selected
function language_changed(e) {
	const chosen_locale = language_selector.options[language_selector.selectedIndex].value.split('.')[0]

	settings['locale'] = chosen_locale

	locale_js.load_locale(chosen_locale)

	init_settings_elements()

	resize_select(e)
}

// the week-start was changed
function resize_select(e) {
	if (e.id !== "") {
		// refresh_calendars(start_calendar.getRange())
		$('#width_tmp_option').html($('#' + e.id + ' option:selected').text())
		e.style.width = $('#width_tmp_select').width() + 4
	}
}

function write_settings_to_file (settings_json = settings) {
	fs.writeFileSync(home + '/config/settings.json', JSON.stringify(settings_json, null, 4))
}

// the week start was changed
function week_start_changed(e) {
	settings['custom_week_start'] = e.selectedIndex

	resize_select(e)
	refresh_calendars()
	init_settings_elements()
}

// the week_start_enable slider was changed
function week_start_slider_changed(e) {
	if (e.checked) {
		week_start_selector.classList.remove("disabled")
	} else {
		week_start_selector.classList.add("disabled")
	}

	settings["custom_week_start_enable"] = e.checked

	week_start_selector.disabled = ! e.checked

	refresh_calendars()
}

// show the menu to choose the regular export days
function choose_regular_day(e) {
	if (e.path[0] === reg_export_days_select_dummy) {
		if (reg_export_days_select.style.display === "unset") {
			reg_export_days_select.style.display = "none"	
		} else {
			reg_export_days_select.style.display = "unset"
		}
	}
}

// a reg-week-day-selector was clicked
function reg_week_day_selector(e) {
	// store the locale in a var, so it can be updated
	var locale = require(path.join(rsc, 'locale', settings['locale']))

	var selected_day = e.path[2].textContent.replaceAll(/\s/g,'')

	settings['export_days'][locale["days"].indexOf(selected_day)] = e.path[0].checked
}

// repopulate the reg-day-selector
function init_reg_day_selector() {
	// store the locale in a var, so it can be updated
	var locale = require(path.join(rsc, 'locale', settings['locale']))

	reg_export_days_select.innerHTML = "";
	for (var i = 0; i < 7; i++) {
		var checkcircle = document.createElement('label')
			checkcircle.classList.add('checkcircle')
		checkcircle.innerHTML = `
			<input type="checkbox" onchange="settings_js.reg_week_day_selector(event)">
			<div class="checkcircle"></div>
		`
	
		if (settings['custom_week_start_enable']) {
			var temp_day_index = settings['custom_week_start']
		} else {
			var temp_day_index = locale['week_start']
		}

		checkcircle.innerHTML += locale['days'][(temp_day_index + i) % 7]

		var day_element = document.createElement('span')
		day_element.appendChild(checkcircle)

		reg_export_days_select.appendChild(day_element)
	}
}

// adds all available locales to the settings dropdown list
function init_settings_elements(select_language) {
	// populate the locale selector
	language_selector.innerHTML = ""
	locale_files.forEach(e => {
		var locale_option = document.createElement('option')
		var temp_locale = require(path.join(rsc, 'locale', e))

		locale_option.innerHTML = temp_locale['lang_name']
		locale_option.value = e.split('.')[0]
		
		language_selector.add(locale_option)
	})
	
	language_selector.value = settings['locale']

	resize_select(language_selector)

	// store the locale in a var, so it can be updated
	var locale = require(path.join(rsc, 'locale', settings['locale']))

	// write the date-insert-key from the settings_file
	var date_insert_key_input = document.getElementById('date_insert_key_input')
	date_insert_key_input.value = settings["date_insert_key"]

	// populate the week-start-selector
	week_start_selector.innerHTML = ""
	locale['days'].forEach(e_date => {
		var day_option = document.createElement('option')
		day_option.innerHTML = e_date
		day_option.value = locale['days'].indexOf(e_date)

		week_start_selector.add(day_option)
	})

	if (!settings["custom_week_start_enable"]) {
		week_start_selector.classList.add("disabled")
	}

	week_start_selector.selectedIndex = settings['custom_week_start']

	week_start_selector.disabled = !settings["custom_week_start_enable"]
	week_start_slider.checked = settings["custom_week_start_enable"]

	resize_select(week_start_selector)

	// refill the date_string
	date_string_input.value = settings["date_string"]

	// populate the reg-export-days-list
	init_reg_day_selector()

	var reg_days = Array.from(reg_export_days_select.getElementsByTagName('label'))

	reg_days.forEach(e => {
		var temp_day = e.textContent.replaceAll(/\s/g,'')
		var temp_day_index = locale['days'].indexOf(temp_day)

		e.getElementsByTagName('input')[0].checked = settings['export_days'][temp_day_index]
	})
}

// initilizes this script
function init() {
	// when the fonts are loaded, resize the selects
	document.fonts.ready.then(() => {
		var selects = Array.from(settings_background_blur.getElementsByTagName('select'))
		
		selects.forEach(e => {
			resize_select(e)
		})
	})

	// fill the setting-elements from the settings_file
	init_settings_elements()
}