const { app, BrowserWindow } = require('electron')
const path = require('path')

require('@electron/remote/main').initialize()

function createWindow() {
	const win = new BrowserWindow({
		// minWidth: 1438,
		// minHeight: 816,

		width: 1438,
		height: 841,
		resizable: false,

		icon: 'icons/favicon/Icon-512x512.png',

		show: false,

		webPreferences: {
			nodeIntegration: true,
			enableRemoteModule: true,
			contextIsolation: false
		},
	});

	win.setMenuBarVisibility(false);

	win.loadFile('./html/main.html');

	win.webContents.on('did-finish-load', () => {
		win.show();
	})

	win.once('close', (e) => {
		win.webContents.executeJavaScript('settings_js.write_settings_to_file()')
	})
}

app.whenReady().then(() => {
	createWindow();

	app.on('activate', () => {
		if (BrowserWindow.getAllWindows().length === 0) {
			createWindow()
		}
	});
});

app.on('window-all-closed', () => {
	if (process.platorm !== 'darwin') {
		app.quit();
	}
});